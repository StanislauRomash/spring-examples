package com.sromash.landon.service;

import com.sromash.landon.modules.Position;
import com.sromash.landon.modules.StaffMember;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StaffService {

    private static final List<com.sromash.landon.modules.StaffMember> staffMembers = List.of(
            new com.sromash.landon.modules.StaffMember(UUID.randomUUID().toString(), "Roy", "Adams", Position.HOUSEKEEPING),
            new com.sromash.landon.modules.StaffMember(UUID.randomUUID().toString(), "Martin", "Adams", Position.SECURITY),
            new com.sromash.landon.modules.StaffMember(UUID.randomUUID().toString(), "Roger", "Alvarez", Position.FRONT_DESK),
            new com.sromash.landon.modules.StaffMember(UUID.randomUUID().toString(), "Anne", "Alvarez", Position.HOUSEKEEPING)
    );

    public List<StaffMember> getAllStaff() {
        return staffMembers;
    }
}
