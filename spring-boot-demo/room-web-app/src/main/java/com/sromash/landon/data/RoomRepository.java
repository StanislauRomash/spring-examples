package com.sromash.landon.data;

import com.sromash.landon.modules.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
}
