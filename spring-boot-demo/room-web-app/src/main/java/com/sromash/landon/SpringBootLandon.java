package com.sromash.landon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootLandon {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootLandon.class, args);
    }
}