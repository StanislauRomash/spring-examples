package com.sromash.clr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    @Bean
    public CommandLineRunner run() {
        return args -> {
            for (int i = 0; i <= 100; i++) {
                if (i % (3 * 5) == 0) {
                    LOG.info("FizzBuzz");
                    continue;
                }
                if (i % 3 == 0) {
                    LOG.info("Fizz");
                    continue;
                }
                if (i % 5 == 0) {
                    LOG.info("Buzz");
                    continue;
                }
                LOG.info(String.valueOf(i));
            }
        };
    }


    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

}