package com.sromash.landon.service;

import com.sromash.landon.data.StaffRepository;
import com.sromash.landon.modules.Position;
import com.sromash.landon.modules.StaffMember;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class StaffService {

    private final StaffRepository staffRepository;

    public StaffService(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    public List<StaffMember> getAllStaff() {
        return staffRepository.findAll();
    }
}
