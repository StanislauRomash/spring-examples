package com.sromash.landon.data;

import com.sromash.landon.modules.StaffMember;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface StaffRepository.
 * <p>
 * Copyright (C) 2024 Edmunds.com
 * <p>
 * Date: 01 16, 2024
 *
 * @author Stanislau Romash
 */
public interface StaffRepository extends JpaRepository<StaffMember, String> {
}
